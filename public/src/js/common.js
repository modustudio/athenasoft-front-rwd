$(function(){
  setTimeout(function(){
    $('.wrapper').animate({'opacity':'1'});
    $(window).trigger('resize');
  },50);
});

$(window).on("load resize", function () {
  if (navigator.userAgent.match(/Android|Mobile|iP(hone|od|ad)|BlackBerry|IEMobile|Kindle|NetFront|Silk-Accelerated|(hpw|web)OS|Fennec|Minimo|Opera M(obi|ini)|Blazer|Dolfin|Dolphin|Skyfire|Zune/)) {
    $('body').addClass('device-mobile')
  } else {
    $('body').removeClass('device-mobile')
  }
  if ($(document).width() <= 1024) {
    $('body').addClass('media-mobile');
  } else {
    $('body').removeClass('media-mobile');
  }
}).resize();

// GNB
$(function () {
  var gnb = '.site-gnb';
  var gnbList = '.site-gnb__list';
  var gnbItem = '.site-gnb__item';
  var gnbLink = '.site-gnb__link';
  var header = '.site-header';

  $(gnbList).on('mouseenter', function () {
    if ($('body').hasClass('media-mobile') || $('body').hasClass('device-mobile')) return;
    $(gnb).addClass('is-over');
    $(header).addClass('is-over');
  });
  $(gnbList).on('mouseleave', function () {
    if ($('body').hasClass('media-mobile') || $('body').hasClass('device-mobile')) return;
    $(gnb).removeClass('is-over')
    $(header).removeClass('is-over')
  });
  $(gnbList).find('a').on('focusin', function () {
    if ($('body').hasClass('media-mobile') || $('body').hasClass('device-mobile')) return;
    $(this).closest(gnbList).trigger('mouseenter');
  });
  $(gnbList).find('a:last').on('focusout', function () {
    if ($('body').hasClass('media-mobile') || $('body').hasClass('device-mobile')) return;
    $(this).closest(gnbList).trigger('mouseleave');
  });

  $(gnbLink).on('click', function(e) {
    if ($('body').hasClass('media-mobile') || $('body').hasClass('device-mobile')){
      if ($(this).closest(gnbItem).find('ul').length == 0) return;
      e.preventDefault();
      if ($(this).closest(gnbItem).hasClass('is-over')) {
        $(this).closest(gnbItem).removeClass('is-over');
      } else {
        $(this).closest(gnbItem).addClass('is-over');
      }
    }
  });
  $(window).resize(function () {
    if (!$('body').hasClass('media-mobile')) {
      $(gnb).removeClass('is-over');
      $(header).removeClass('is-over');
    };
  });
});

// mobile menu
$(function () {
  $(document).on('click','.mobile-button-open, .mobile-button-close',function (e) {
    if ($('.mobile-menu').hasClass('is-on')) {
      $('.mobile-menu').removeClass('is-on');
      $('.mobile-menu-dimmed').remove();
    } else {
      $('.mobile-menu').addClass('is-on');
      $('<div class="mobile-menu-dimmed"></div>').appendTo($(document.body))
    }
  })
  $(window).resize(function () {
    if (!$('body').hasClass('media-mobile')) {
      $('.mobile-menu').removeClass('is-on');
      $('.mobile-menu-dimmed').remove();
      $('.mobile-button-open').attr('disabled','disabled');
      $('.mobile-button-close').attr('disabled','disabled');
    } else {
      $('.mobile-button-open').removeAttr('disabled');
      $('.mobile-button-close').removeAttr('disabled');
    }
  });
});

// page location menu
$(function () {
  var locationItem = '.u-page-location__item';
  var locationList = '.u-page-list';

  $(locationItem).on('mouseenter', function () {
    if ($('body').hasClass('media-mobile') || $('body').hasClass('device-mobile')) return;
    $(this).find(locationList).addClass('is-over');
  });
  $(locationItem).on('mouseleave', function () {
    if ($('body').hasClass('media-mobile') || $('body').hasClass('device-mobile')) return;
    $(this).find(locationList).removeClass('is-over');
  });
  $(locationItem).find('a').on('focusin', function () {
    if ($('body').hasClass('media-mobile') || $('body').hasClass('device-mobile')) return;
    $(this).closest(locationItem).trigger('mouseenter');
  });
  $(locationItem).find('a:last').on('focusout', function () {
    if ($('body').hasClass('media-mobile') || $('body').hasClass('device-mobile')) return;
    $(this).closest(locationItem).trigger('mouseleave');
  });
  $(locationItem).on('click', function(e) {
    if ($('body').hasClass('media-mobile') || $('body').hasClass('device-mobile')){
      if ($(this).find(locationList).length == 0) return;
      if ($(this).find(locationList).hasClass('is-over')) {
        $(this).find(locationList).removeClass('is-over');
      } else {
        $(this).find(locationList).addClass('is-over');
        $(this).siblings().find(locationList).removeClass('is-over');
      }
    }
  });
  $(document).on('click touchend', function(e) {
    if ($('body').hasClass('media-mobile') || $('body').hasClass('device-mobile')) {
      if ($(locationList).hasClass('is-over') && $(e.target).siblings(locationList).hasClass('is-over') == false && $(e.target).hasClass('u-page-list__link') == false && $(e.target).siblings(locationList).length == 0) {
        $(locationList).removeClass('is-over');
      }
    }
  });
});

// tab
var tab = function() {
  if ($("[data-ui-tab='true']").length > 0) {
    $("[data-ui-tab='true']").each(function() {
      var $tab = $(this);
      var hash = window.location.hash;
      var tabArray= [];
      $tab.find("a[href^=#]").each(function(idx){
        tabArray.push($tab.find("a[href^=#]").eq(idx).attr("href"));
      });
      for (var i in tabArray) {
        $(tabArray[i]).removeClass("is-show").addClass("is-hide");
      }

      if (hash && tabArray.indexOf(hash) != -1) {
        sele($tab.find("a[href="+hash+"]"),"is-on","is-on");
        $(hash).removeClass("is-hide").addClass("is-show");
      }  else if ($tab.find("[data-ui-full]").length == 1 ) {
        sele($tab.find("a").eq(0),"is-on","is-on");
        for (var i in tabArray) $(tabArray[i]).removeClass("is-hide").addClass("is-show");
      } else {
        if ($tab.children().hasClass("is-on")) {
          $($tab.children(".is-on").find("a[href^=#]").attr("href")).removeClass("is-hide").addClass("is-show");
        } else {
          sele($tab.find("a[href^=#]").eq(0),"is-on","is-on");
          $(tabArray[0]).removeClass("is-hide").addClass("is-show");
        }
      }
    });
  }
  $(document).on("click","[data-ui-tab='true'] a[href^=#]",function(e){
    e.preventDefault();

    var $tab = $(this).closest("[data-ui-tab='true']");
    var tabArray= [];
    $tab.find("a[href^=#]").each(function(idx){
      tabArray.push($tab.find("a[href^=#]").eq(idx).attr("href"));
    });

    sele($(this),"is-on","is-on");
    if ($(this).data("ui-full") == true) {
      for (var i in tabArray) $(tabArray[i]).removeClass("is-hide").addClass("is-show");
    } else {
      if ($tab.data("ui-hash")) {
        if (!$(this).parent().hasClass("is-on")) window.location.hash = ($(this).attr("href"));
      }
      for (var i in tabArray) $(tabArray[i]).removeClass("is-show").addClass("is-hide");
      $($(this).attr("href")).removeClass("is-hide").addClass("is-show");
    }

    if (!$(e.target).hasClass("js-state") && $('.js-list').hasClass('is-open') && $(e.target).closest($('.js-list')).hasClass('is-open') == true) {
      $('.js-list').removeClass('is-open');
      $('.js-state').removeClass('is-open');
    }
  });
  function sele(el,show,hide) {
    $(el).parent().addClass(show).siblings().removeClass(hide);
  }
};
$(function() {
  tab();
});

// dropDown
$(function(){
  var $dropDown = $("[data-drop-down]");
  var $state = $dropDown.find(".js-state");
  var $list = $dropDown.find(".js-list");
  var toggoleOpen = "is-open";

  if($dropDown.find(".is-on").length > 0){
    var onTab = $dropDown.find(".is-on").children('a').text();
    $state.find('.u-tab2__state-name').html(onTab);
  }
  $state.on("click keypress",function(e){
    if ((e.keyCode == 13)||(e.type == "click")) {
      if ($(this).hasClass(toggoleOpen)) {
        $(this).removeClass(toggoleOpen).siblings($list).removeClass(toggoleOpen);
      } else {
        $(this).addClass(toggoleOpen).siblings($list).addClass(toggoleOpen);
      }
    }
  });
  $(document).on("click",function(e){
    if (!$(e.target).hasClass("js-state") && $list.hasClass(toggoleOpen) && $(e.target).closest($list).hasClass(toggoleOpen) == false) {
      $list.removeClass(toggoleOpen);
      $state.removeClass(toggoleOpen);
    }
    if($(e.target).hasClass('js-link') === true){
      $state.find('.u-tab2__state-name').html($(e.target).text());
    }
  });
});

// effect
$(function(){
  $('.js-view').each(function(){
    var $this = $(this);
    if ($this.offset().top < $(window).scrollTop() + $(window).height() && $this.offset().top > $(window).scrollTop() - $this.height()) {
      $this.addClass('is-view')
    } else {
      var n = function() {
        if ($this.offset().top < $(window).scrollTop() + $(window).height() && $this.offset().top > $(window).scrollTop() - $this.height()) {
          $this.addClass('is-view')
          $(window).unbind('scroll', n);
        }
      }
      $(window).bind('scroll', n);
      
    }
  });
});