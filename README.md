# default 사이트

## `gulp` 셋팅
```
npm i -D gulp gulp-plumber@1.2.0 gulp-connect@5.0.0 gulp-ejs-locals@0.0.1 gulp-sass@3.1.0 gulp-csscomb@3.0.8 gulp-autoprefixer@4.1.0 gulp-jshint@2.1.0 jshint-stylish@2.2.1 gulp-watch@5.0.0 gulp-clean@0.4.0 gulp-prettify@0.5.0
npm i -D gulp-imagemin@4.1.0
npm i -D imagemin-pngquant@5.0.1
```
`gulp -v` 으로 *CLI*, *Local* 동일 버젼 확인  

## 폴더 구조
    public/
      src/
        css/
        ejs/
        images/
        fonts/
        js/
        libs/
        scss/
        index.html(index.ejs)
    .csscomb.json
    .gitignore
    gulpfile.js
    package-lock.json
    package.json
    README.md  