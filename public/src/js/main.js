// fullpage
$(document).ready(function() {
  $('.site-main').fullpage({
    sectionSelector: '.js-section',
    scrollBar: true,
    navigation: true,
    navigationPosition: 'right',
    navigationTooltips: ['Main', 'Business', 'Solution', 'About Us', 'Contact Us'],
    responsiveWidth: 1024,
    responsiveHeight: 700
  });
});

// visual
$(function() {
  var $mainVisualSlider = $('.js-main-visual');
  if( $mainVisualSlider.find('.js-main-visual-item:not(.bx-clone)').length > 1 ){
    $mainVisualSlider.bxSlider({
      wrapperClass: 'main-visual-wrap',
      auto: true,
      speed: 1000,
      pause: 8000,
      easing: 'ease-out',
      pager: true,
      controls: false,
      infiniteLoop: true,
      hideControlOnEnd: true,
      touchEnabled:true,
      swipeThreshold: 100,
      responsive: true,
      onSliderLoad: function(currentIndex){
        timer = setTimeout(null, 10000);
        $('.js-main-visual-item:not(.bx-clone)').eq(currentIndex).addClass('is-active')
      },
      onSlideBefore: function() {
          clearTimeout(timer);
      },
      onSlideAfter: function($slideElement, oldIndex, newIndex){
        timer = setTimeout(this.startAuto(), 8000);
        $slideElement.addClass('is-active').siblings().removeClass('is-active')
      }

    });
  }
});

// main-mini-slide
$(function() {
  var $mainMiniSlider = $('.js-main-mini-slide');
  if( $mainMiniSlider.find('.js-main-mini-slide-item:not(.bx-clone)').length > 1 ){
    $mainMiniSlider.bxSlider({
      wrapperClass: 'main-mini-slide-wrap',
      auto: true,
      speed: 1000,
      pause: 6000,
      easing: 'ease-out',
      pager: true,
      controls: false,
      infiniteLoop: true,
      hideControlOnEnd: true,
      touchEnabled:true,
      swipeThreshold: 100,
      responsive: true,
      onSliderLoad: function(currentIndex){
        timer = setTimeout(null, 10000);
        $('.js-main-mini-slide-item:not(.bx-clone)').eq(currentIndex).addClass('is-active')
      },
      onSlideBefore: function() {
          clearTimeout(timer);
      },
      onSlideAfter: function($slideElement, oldIndex, newIndex){
        timer = setTimeout(this.startAuto(), 6000);
        $slideElement.addClass('is-active').siblings().removeClass('is-active')
      }
    });
  }
});